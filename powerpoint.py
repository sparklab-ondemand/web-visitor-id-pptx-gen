__author__ = 'BettlerP'

# from pptx.util import Inches
# from pptx.enum.chart import XL_CHART_TYPE
# from pptx.enum.shapes import MSO_SHAPE_TYPE
from pptx import Presentation
from pptx.chart.data import ChartData
from pptx.util import Pt


# update a power point table structure with a value
def updateTable(table,row,col,val):
    # change cell in table
    cell = table.rows[row].cells[col]
    para = cell.text_frame.paragraphs[0]
    para.clear()
    run = para.add_run()
    # format $ values
    valD = val
    if '$' in val:
        valD = '$'+"{:,}".format(int(val[1:]))

    run.text = valD
    font = run.font
    font.name = 'Arial'
    font.size = Pt(13)
    font.bold = False

def updateNumber(shape,val, run, before='', after=''):
    shape.text_frame.paragraphs[0].runs[run].text = before+("{:,}".format(int(val)))+after

def updateNumberPercent(shape,val, run, before='', after='', precision=1):
    shape.text_frame.paragraphs[0].runs[run].text = before+(("{:.0"+str(precision)+"%}").format(val))+after



def processCsvToPPTX(data, templateFile, outputFile):

    # load template file
    f = open(templateFile)
    prs = Presentation(f)
    f.close()

    print 'Processing ' + outputFile

    # cover slide (1)
    prs.slides[0].shapes[0].text = "Web Traffic Report \nPrepared for "+data['company']['name']
    prs.slides[0].shapes[2].text = data['date']

    # donut slide (3)
    currentSlide = 3
    donut = prs.slides[currentSlide].shapes[2]
    cData = ChartData()
    cData.categories = ['ISP', 'BIZ', 'NO']
    cData.add_series('1', (data['stats']['overview']['ISP'], data['stats']['overview']['BIZ'], data['stats']['overview']['NOMATCH']))
    donut.chart.replace_data(cData)

    unique = prs.slides[currentSlide].shapes[3]
    updateNumber(unique,data['stats']['overview']['Unique'],1,'(',')')

    countISP = float(data['stats']['overview']['ISP'])
    countBIZ = float(data['stats']['overview']['BIZ'])
    countNO  = float(data['stats']['overview']['NOMATCH'])
    countUni = float(data['stats']['overview']['Unique'])

    matchISPNum  = prs.slides[currentSlide].shapes[8]
    updateNumber(matchISPNum,data['stats']['overview']['ISP'],0)
    matchISPNumP = prs.slides[currentSlide].shapes[9]
    updateNumberPercent(matchISPNumP,countISP/countUni,0)

    matchBIZNum  = prs.slides[currentSlide].shapes[10]
    updateNumber(matchBIZNum,data['stats']['overview']['BIZ'],0)
    matchBIZNumP = prs.slides[currentSlide].shapes[11]
    updateNumberPercent(matchBIZNumP,countBIZ/countUni,0)


    mTot = int(data['stats']['overview']['BIZ'])+int(data['stats']['overview']['ISP'])
    matchTotNum  = prs.slides[currentSlide].shapes[12]
    updateNumber(matchTotNum,str(mTot),0)
    matchTotNumP = prs.slides[currentSlide].shapes[13]
    mTotP = (countBIZ+countISP)/countUni
    if len(matchTotNumP.text_frame.paragraphs[0].runs)>1:
        matchTotNumP.text_frame.paragraphs[0].runs[1].text = ""
    updateNumberPercent(matchTotNumP,mTotP,0)

    matchNoNum  = prs.slides[currentSlide].shapes[14]
    updateNumber(matchNoNum,data['stats']['overview']['NOMATCH'],0)
    matchNoNumP = prs.slides[currentSlide].shapes[15]
    updateNumberPercent(matchNoNumP,countNO/countUni,0)

    website = prs.slides[currentSlide].shapes[22].text_frame.paragraphs[0].runs[0].text = data['company']['site']
    totalRecs = prs.slides[currentSlide].shapes[24]
    updateNumber(totalRecs,data['stats']['overview']['Total'],0)
    uniqueRecs = prs.slides[currentSlide].shapes[26]
    updateNumber(uniqueRecs,data['stats']['overview']['Unique'],0)

    currentSlide = 4;
    # slide 4 vertical chart: visit by region
    chart = prs.slides[currentSlide].shapes[4]
    cData = ChartData()
    cData.categories = ['Europe', 'North America', 'Asia', 'Africa', 'South America', 'Oceania']
    cData.add_series('1', data['stats']['regionBiz'])
    cData.add_series('2', data['stats']['regionIsp'])
    chart.chart.replace_data(cData)

     # update percentages
    percentRegion = []
    for i,v in enumerate(data['stats']['regionIsp']):
        v2 = float(data['stats']['regionBiz'][i])
        if float(v)+v2>0:
            percentRegion.append(v2/(float(v)+v2))
        else:
            percentRegion.append(0)

    percentEU = prs.slides[currentSlide].shapes [6]; updateNumberPercent(percentEU, percentRegion[0], 0, '', '', 0)
    percentNA = prs.slides[currentSlide].shapes [7]; updateNumberPercent(percentNA, percentRegion[1], 0, '', '', 0)
    percentAS = prs.slides[currentSlide].shapes [8]; updateNumberPercent(percentAS, percentRegion[2], 0, '', '', 0)
    percentAF = prs.slides[currentSlide].shapes [9]; updateNumberPercent(percentAF, percentRegion[3], 0, '', '', 0)
    percentSA = prs.slides[currentSlide].shapes[10]; updateNumberPercent(percentSA, percentRegion[4], 0, '', '', 0)
    percentOC = prs.slides[currentSlide].shapes[11]; updateNumberPercent(percentOC, percentRegion[5], 0, '', '', 0)

    currentSlide = 5
    # slide 5 horizontal chart: top visiting countries
    chart = prs.slides[currentSlide].shapes[4].chart
    cData = ChartData()
    if len(data['stats']['country'])<10:
        for i in range(len(data['stats']['country']),10):
            data['stats']['country'].insert(0,0)
            data['stats']['countryn'].insert(0,"")
    cData.categories = data['stats']['countryn']
    cData.add_series('1', data['stats']['country'])
    chart.replace_data(cData)

    percentCountry = reversed((12,14,16,18,20,22,24,26,28,30))
    for i,p in enumerate(percentCountry):
        per = prs.slides[currentSlide].shapes[p]
        perc = 0
        if i<len(data['stats']['country']):
            perc = float(data['stats']['country'][i])/countBIZ
        updateNumberPercent(per, perc, 0, '', '', 0)

    currentSlide = 6
    # slide 6 horizontal chart: company size
    chart = prs.slides[currentSlide].shapes[4].chart
    cData = ChartData()
    cData.categories = ['Blank', '1 - 10', '11 - 50', '51 - 100', '101 - 500', '501 - 1,000',
                        '1,001 - 10,000', '10,001 - 50-000', '50,001 - 100,000', '100,001 - 150,000', '150,001+']
    cData.add_series('1', list(reversed(data['stats']['size'])))
    chart.replace_data(cData)

    percentSize = (12,14,16,18,20,22,24,26,28, 30, 32)
    for i,p in enumerate(percentSize):
        per = prs.slides[currentSlide].shapes[p]
        updateNumberPercent(per,float(data['stats']['size'][i])/countBIZ, 0, '', '', 0)



    # slide 7 top industries by rev table
    currentSlide = 7
    table = prs.slides[currentSlide].shapes[2].table

    for r, row in enumerate(data['stats']['top-ind-rev']):
        for c, cell in enumerate(row):
            if c==2:
                cell = (cell[:57] + '..') if len(cell) > 59 else cell
            updateTable(table, r, c, cell)

    currentSlide = 8
    # slide 8 top industries by rev table
    table = prs.slides[currentSlide].shapes[2].table
    for r, row in enumerate(data['stats']['top-ind-fort']):
        for c, cell in enumerate(row):
            if c==2:
                cell = (cell[:57] + '..') if len(cell) > 59 else cell
            updateTable(table, r, c, cell)

    # slide 9 top industries by rev table
    currentSlide=9
    table = prs.slides[currentSlide].shapes[2].table
    for r, row in enumerate(data['stats']['top-ind-forb']):
        for c, cell in enumerate(row):
            if c==2:
                cell = (cell[:57] + '..') if len(cell) > 59 else cell
            updateTable(table, r, c, cell)

    prs.save('temp.pptx')

    f = open('temp.pptx')
    prs = Presentation(f)
    f.close()

    # realign charts
    # for 2 digit max total matches
    currentSlide = 4
    if mTot<90:
        prs.slides[currentSlide].shapes[4].height = 4904506
        prs.slides[currentSlide].shapes[4].width = 6590774
        prs.slides[currentSlide].shapes[4].left  = 2250072
    currentSlide = 5
    chart = prs.slides[currentSlide].shapes[4].height = 4604006
    prs.save(outputFile)
    print 'File is ready: ' + outputFile
