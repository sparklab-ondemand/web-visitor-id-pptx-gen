# README #

Pptx-gen is used for producing an analytics report for Web visitor ID prospect.
It is a Python script that converts an input analytics csv file to a Windows PowerPoint file.

### What is this repository for? ###

The repos includes:

+ the main Python file
+ an input test file
+ the input pptx template file

### How do I get set up? ###

* Summary of set up: TBC
* Configuration: TBC
* Dependencies: pptx, argparse, csv
* Database configuration: N/A
* How to run tests: python main.py test.out.stat.csv
* Deployment instructions: TBC

### Who do I talk to? ###

* Project owner: Philippe Bettler
* Stake Holder: Nelrose Viloria