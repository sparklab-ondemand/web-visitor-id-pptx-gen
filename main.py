__author__ = 'BettlerP'



from powerpoint import processCsvToPPTX
import argparse
import csv

# load the analytics data from the csv file
def readCsvStat(file):
    data = {}
    data['company'] = {}
    data['stats'] = {}
    data['stats']['overview'] = {}
    data['stats']['top-ind-rev'] = []
    data['stats']['top-ind-fort'] = []
    data['stats']['top-ind-forb'] = []
    with open(file,'rU') as csvfile:
        csvdata = csv.reader(csvfile, delimiter=",")
        for i, row in enumerate(csvdata):
            if i==0:
                data['company']['name'] = row[0]
                data['date'] = row[1]
                data['company']['site'] = row[2]
            if i==2:
                data['stats']['overview']['Total']   = row[0]
                data['stats']['overview']['Unique']  = row[1]
                data['stats']['overview']['ISP']     = row[2]
                data['stats']['overview']['BIZ']     = row[3]
                data['stats']['overview']['NOMATCH'] = row[4]
            if i==4:
                data['stats']['regionIsp'] = map(lambda x: int(x), row[0:6])
            if i==5:
                data['stats']['regionBiz'] = map(lambda x: int(x), row[0:6])
            if i==7:
                data['stats']['countryn'] = map(lambda code: getCountryName(code),row[0:10])
            if i==8:
                data['stats']['country'] = map(lambda x: int(x), row[0:10])
            if i==10:
                data['stats']['size'] = map(lambda x: int(x), row[1:12])
            if i>=12 and i<=21:
                data['stats']['top-ind-rev'].append(row[0:5])
            if i>=23 and i<=32:
                data['stats']['top-ind-fort'].append(row[0:5])
            if i>=34 and i<=43:
                data['stats']['top-ind-forb'].append(row[0:5])
    return data
# map country codes to country names
def getCountryName(code):
    codeMap = {
        '':'(Blank)',
        'AU': 'Australia',
        'BD':'Bangladesh',
        'BE':'Belgium',
        'BR': 'Brasil',
        'CA': 'Canada',
        'CH': 'Switzerland',
        'CN':'China',
        'CZ': 'Czech Republic',
        'DE': 'Germany',
        'DK': 'Denmark',
        'FR': 'France',
        'GB': 'Great Britain',
        'HK': 'Hong Kong',
        'IE': 'Ireland',
        'IN': 'India',
        'IT': 'Italy',
        'JP': 'Japan',
        'KR': 'South Korea',
        'LU': 'Luxembourg',
        'NL': 'Netherlands',
        'PL': 'Poland',
        'RU': 'Russia',
        'SE': 'Sweden',
        'TW':'Taiwan',
        'US': 'United States'
    }
    return codeMap.get(code,code)


# file = '../ip2duns-batch/customers/Altares/IP File-Test.out.stat.csv'

parser = argparse.ArgumentParser(description='From an analytics csv file, produce a web visitor ID report.')
parser.add_argument('file', type=str, metavar='input_file.csv', help='The csv input file to be processed')
parser.add_argument('--t', type=str, metavar='template_file.pptx', help='The template file to be used (pptx format)', default='WTR-Template.pptx')
args = parser.parse_args()

try:
    data = readCsvStat(args.file)
except IOError:
    print('Error: cannot find '+ args.file)
    quit()

processCsvToPPTX(data, args.t, args.file.replace('.csv','.pptx'))

